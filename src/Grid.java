/*Author: Eloise Corona 45259739
  COMP229 Assignment 1 2018
  21/8/18/ */

import java.awt.*;
import java.util.*;

public class Grid {

    private Cell[][] cells  = new Cell[20][20];; //creates an array of cells that is 20x20

    private int x; //stores coordinates of the cell
    private int y;

    public Grid(int x, int y) { //Grid constructor
        this.x = x;
        this.y = y;

        for(int i = 0; i < 20; i++) {
            for(int j = 0; j < 20; j++) {
                cells[i][j] = new Cell(x + j * 35, y + i * 35); //populate cells with coords for grid
            }
        }
    }

    public void paint(Graphics g, Point mousePosition) { //paint method to draw the cells to create grid
        for(int y = 0; y < 20; ++y) {
            for(int x = 0; x < 20; ++x) {
                Cell thisCell = cells[x][y];
                thisCell.paint(g, thisCell.contains(mousePosition)); //adds grid to the window and makes mouse visible
            }
        }
    }}
