/*Author: Eloise Corona 45259739
  COMP229 Assignment 1 2018
  21/8/18/ */

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.*;
import java.util.*;

public class Main extends JFrame implements Runnable {

    private class Canvas extends JPanel {

        private Grid grid;

        public Canvas() {
            setPreferredSize(new Dimension(1280, 720)); //sets the size of the window

            grid = new Grid(10, 10); //creates a grid
        }

        @Override
        public void paint(Graphics g) {
            grid.paint(g, getMousePosition()); //paints grid and keeps mouse visible
        }
    }

    public static void main(String[] args) {
        Main window = new Main();
        window.run(); //opens and runs the window
    }

    private Main() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //makes program stop when window is closed
        this.setContentPane(new Canvas());
        this.pack();
        this.setVisible(true); //sets window to be visible
    }

    @Override
    public void run() {
        while (true) {
            this.repaint();
        }
    }

}
