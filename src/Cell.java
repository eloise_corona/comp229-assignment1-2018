/*Author: Eloise Corona 45259739
  COMP229 Assignment 1 2018
  21/8/18/ */

import java.awt.*;
import java.util.*;

public class Cell extends Rectangle{ //inherits from Rectangle class

    public int grassHeight = 0; //variable to store height of grass
    public Color cellColour;

    Random r = new Random(); //creates a random object, from java.util, to use for randomised colours
    Random gr = new Random(); //used gr instead of g as g is used for graphics object
    Random b = new Random();

    public Cell(int x,int y){
        super(x,y, 35, 35); //passes x and y values onto superclass Rect constructor

        int greenColour = gr.nextInt(128) + 128;//creates variable for green value
        int redColour = r.nextInt(56); //value for red
        int blueColour = b.nextInt(56); //value for blue
        cellColour = new Color(redColour, greenColour, blueColour); //creates colour object to store rgb colour in

        if(greenColour <= 50){ //checks size of grass and sets grassHeight to appropriate height
            grassHeight = 1;
        }else if(greenColour > 50 && greenColour <= 100){
            grassHeight = 2;
        }else if(greenColour > 100 && greenColour <= 150){
            grassHeight = 3;
        }else if(greenColour > 150 && greenColour <= 200){
            grassHeight = 4;
        }else {
            grassHeight = 5;
        }
    }
    
    public void paint(Graphics g, boolean highlighted){
        g.setColor(cellColour);//sets colour to fill rect
        g.fillRect(x, y, 35, 35);//fills rect

        g.setColor(Color.DARK_GRAY);//sets colour to fill rect
        g.drawRect(x, y, 35, 35);//outlines rect
    }

    public boolean contains(Point target) {
        if(target == null)
            return false;

        return super.contains(target); //calls contains method from  superclass to check that x & y aren't null
        }
}
